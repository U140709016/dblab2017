#initial time :0.375 sec
#after index : 0.188 sec
#after PK :0.079
use uniprot_example;

select * from proteins;

load data  local infile 'C:\\Users\\betul\\Insert.txt' into table proteins fields terminated by '|';

select *
from proteins 
where protein_name like "%tumor%" and uniprot_id like "%human%" 
order by uniprot_id; 

create index uniprot_index on proteins (uniprot_id);
drop index uniprot_index on proteins ;

alter table proteins add constraint pk_proteins primary key (uniprot_id) ;